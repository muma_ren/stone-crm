-- ----------------------------
-- 1、部门表
-- ----------------------------
drop table if exists sys_dept;
create table sys_dept (
  dept_id           bigint(20)      not null auto_increment    comment '部门id',
  parent_id         bigint(20)      default 0                  comment '父部门id',
  ancestors         varchar(50)     default ''                 comment '祖级列表',
  dept_name         varchar(30)     default ''                 comment '部门名称',
  order_num         int(4)          default 0                  comment '显示顺序',
  leader            varchar(20)     default null               comment '负责人',
  phone             varchar(11)     default null               comment '联系电话',
  email             varchar(50)     default null               comment '邮箱',
  status            char(1)         default '0'                comment '部门状态（0正常 1停用）',
  del_flag          char(1)         default '0'                comment '删除标志（0代表存在 2代表删除）',
  create_by         varchar(64)     default ''                 comment '创建者',
  create_time 	    datetime                                   comment '创建时间',
  update_by         varchar(64)     default ''                 comment '更新者',
  update_time       datetime                                   comment '更新时间',
  primary key (dept_id)
) engine=innodb auto_increment=200 comment = '部门表';

-- ----------------------------
-- 初始化-部门表数据
-- ----------------------------
insert into sys_dept values(100,  0,   '0',          '若依科技',   0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', sysdate(), '', null);
insert into sys_dept values(101,  100, '0,100',      '深圳总公司', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', sysdate(), '', null);
insert into sys_dept values(102,  100, '0,100',      '长沙分公司', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', sysdate(), '', null);
insert into sys_dept values(103,  101, '0,100,101',  '研发部门',   1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', sysdate(), '', null);
insert into sys_dept values(104,  101, '0,100,101',  '市场部门',   2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', sysdate(), '', null);
insert into sys_dept values(105,  101, '0,100,101',  '测试部门',   3, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', sysdate(), '', null);
insert into sys_dept values(106,  101, '0,100,101',  '财务部门',   4, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', sysdate(), '', null);
insert into sys_dept values(107,  101, '0,100,101',  '运维部门',   5, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', sysdate(), '', null);
insert into sys_dept values(108,  102, '0,100,102',  '市场部门',   1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', sysdate(), '', null);
insert into sys_dept values(109,  102, '0,100,102',  '财务部门',   2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', sysdate(), '', null);


-- ----------------------------
-- 2、用户信息表
-- ----------------------------
drop table if exists sys_user;
create table sys_user (
  user_id           bigint(20)      not null auto_increment    comment '用户ID',
  dept_id           bigint(20)      default null               comment '部门ID',
  user_name         varchar(30)     not null                   comment '用户账号',
  nick_name         varchar(30)     not null                   comment '用户昵称',
  user_type         varchar(2)      default '00'               comment '用户类型（00系统用户）',
  email             varchar(50)     default ''                 comment '用户邮箱',
  phonenumber       varchar(11)     default ''                 comment '手机号码',
  sex               char(1)         default '0'                comment '用户性别（0男 1女 2未知）',
  avatar            varchar(100)    default ''                 comment '头像地址',
  password          varchar(100)    default ''                 comment '密码',
  status            char(1)         default '0'                comment '帐号状态（0正常 1停用）',
  del_flag          char(1)         default '0'                comment '删除标志（0代表存在 2代表删除）',
  login_ip          varchar(128)    default ''                 comment '最后登录IP',
  login_date        datetime                                   comment '最后登录时间',
  create_by         varchar(64)     default ''                 comment '创建者',
  create_time       datetime                                   comment '创建时间',
  update_by         varchar(64)     default ''                 comment '更新者',
  update_time       datetime                                   comment '更新时间',
  remark            varchar(500)    default null               comment '备注',
  primary key (user_id)
) engine=innodb auto_increment=100 comment = '用户信息表';

-- ----------------------------
-- 初始化-用户信息表数据
-- ----------------------------
insert into sys_user values(1,  103, 'admin', '若依', '00', 'ry@163.com', '15888888888', '1', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', sysdate(), 'admin', sysdate(), '', null, '管理员');
insert into sys_user values(2,  105, 'ry',    '若依', '00', 'ry@qq.com',  '15666666666', '1', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', sysdate(), 'admin', sysdate(), '', null, '测试员');


-- ----------------------------
-- 3、岗位信息表
-- ----------------------------
drop table if exists sys_post;
create table sys_post
(
  post_id       bigint(20)      not null auto_increment    comment '岗位ID',
  post_code     varchar(64)     not null                   comment '岗位编码',
  post_name     varchar(50)     not null                   comment '岗位名称',
  post_sort     int(4)          not null                   comment '显示顺序',
  status        char(1)         not null                   comment '状态（0正常 1停用）',
  create_by     varchar(64)     default ''                 comment '创建者',
  create_time   datetime                                   comment '创建时间',
  update_by     varchar(64)     default ''			       comment '更新者',
  update_time   datetime                                   comment '更新时间',
  remark        varchar(500)    default null               comment '备注',
  primary key (post_id)
) engine=innodb comment = '岗位信息表';

-- ----------------------------
-- 初始化-岗位信息表数据
-- ----------------------------
insert into sys_post values(1, 'ceo',  '董事长',    1, '0', 'admin', sysdate(), '', null, '');
insert into sys_post values(2, 'se',   '项目经理',  2, '0', 'admin', sysdate(), '', null, '');
insert into sys_post values(3, 'hr',   '人力资源',  3, '0', 'admin', sysdate(), '', null, '');
insert into sys_post values(4, 'user', '普通员工',  4, '0', 'admin', sysdate(), '', null, '');


-- ----------------------------
-- 4、角色信息表
-- ----------------------------
drop table if exists sys_role;
create table sys_role (
  role_id              bigint(20)      not null auto_increment    comment '角色ID',
  role_name            varchar(30)     not null                   comment '角色名称',
  role_key             varchar(100)    not null                   comment '角色权限字符串',
  role_sort            int(4)          not null                   comment '显示顺序',
  data_scope           char(1)         default '1'                comment '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  menu_check_strictly  tinyint(1)      default 1                  comment '菜单树选择项是否关联显示',
  dept_check_strictly  tinyint(1)      default 1                  comment '部门树选择项是否关联显示',
  status               char(1)         not null                   comment '角色状态（0正常 1停用）',
  del_flag             char(1)         default '0'                comment '删除标志（0代表存在 2代表删除）',
  create_by            varchar(64)     default ''                 comment '创建者',
  create_time          datetime                                   comment '创建时间',
  update_by            varchar(64)     default ''                 comment '更新者',
  update_time          datetime                                   comment '更新时间',
  remark               varchar(500)    default null               comment '备注',
  primary key (role_id)
) engine=innodb auto_increment=100 comment = '角色信息表';

-- ----------------------------
-- 初始化-角色信息表数据
-- ----------------------------
insert into sys_role values('1', '超级管理员',  'admin',  1, 1, 1, 1, '0', '0', 'admin', sysdate(), '', null, '超级管理员');
insert into sys_role values('2', '普通角色',    'common', 2, 2, 1, 1, '0', '0', 'admin', sysdate(), '', null, '普通角色');


-- ----------------------------
-- 5、菜单权限表
-- ----------------------------
drop table if exists sys_menu;
create table sys_menu (
  menu_id           bigint(20)      not null auto_increment    comment '菜单ID',
  menu_name         varchar(50)     not null                   comment '菜单名称',
  parent_id         bigint(20)      default 0                  comment '父菜单ID',
  order_num         int(4)          default 0                  comment '显示顺序',
  path              varchar(200)    default ''                 comment '路由地址',
  component         varchar(255)    default null               comment '组件路径',
  query             varchar(255)    default null               comment '路由参数',
  is_frame          int(1)          default 1                  comment '是否为外链（0是 1否）',
  is_cache          int(1)          default 0                  comment '是否缓存（0缓存 1不缓存）',
  menu_type         char(1)         default ''                 comment '菜单类型（M目录 C菜单 F按钮）',
  visible           char(1)         default 0                  comment '菜单状态（0显示 1隐藏）',
  status            char(1)         default 0                  comment '菜单状态（0正常 1停用）',
  perms             varchar(100)    default null               comment '权限标识',
  icon              varchar(100)    default '#'                comment '菜单图标',
  create_by         varchar(64)     default ''                 comment '创建者',
  create_time       datetime                                   comment '创建时间',
  update_by         varchar(64)     default ''                 comment '更新者',
  update_time       datetime                                   comment '更新时间',
  remark            varchar(500)    default ''                 comment '备注',
  primary key (menu_id)
) engine=innodb auto_increment=2000 comment = '菜单权限表';

-- ----------------------------
-- 初始化-菜单信息表数据
-- ----------------------------
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1, '系统管理', 0, 1, 'system', null, '', 1, 0, 'M', 0, 0, '', 'system', 'admin', '2024-04-19 06:51:53', '', null, '系统管理目录');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2, '系统监控', 0, 2, 'monitor', null, '', 1, 0, 'M', 1, 0, '', 'monitor', 'admin', '2024-04-19 06:51:54', 'admin', '2024-04-26 14:49:53', '系统监控目录');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (3, '系统工具', 0, 3, 'tool', null, '', 1, 0, 'M', 0, 0, '', 'tool', 'admin', '2024-04-19 06:51:55', 'admin', '2024-04-26 14:49:59', '系统工具目录');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (100, '用户管理', 1, 1, 'user', 'system/user/index', '', 1, 0, 'C', 0, 0, 'system:user:list', 'user', 'admin', '2024-04-19 06:51:56', '', null, '用户管理菜单');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (101, '角色管理', 1, 2, 'role', 'system/role/index', '', 1, 0, 'C', 0, 0, 'system:role:list', 'peoples', 'admin', '2024-04-19 06:51:57', '', null, '角色管理菜单');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (102, '菜单管理', 1, 3, 'menu', 'system/menu/index', '', 1, 0, 'C', 0, 0, 'system:menu:list', 'tree-table', 'admin', '2024-04-19 06:51:58', '', null, '菜单管理菜单');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (103, '部门管理', 1, 4, 'dept', 'system/dept/index', '', 1, 0, 'C', 0, 0, 'system:dept:list', 'tree', 'admin', '2024-04-19 06:51:59', '', null, '部门管理菜单');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (104, '岗位管理', 1, 5, 'post', 'system/post/index', '', 1, 0, 'C', 0, 0, 'system:post:list', 'post', 'admin', '2024-04-19 06:52:00', '', null, '岗位管理菜单');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (105, '字典管理', 1, 6, 'dict', 'system/dict/index', '', 1, 0, 'C', 0, 0, 'system:dict:list', 'dict', 'admin', '2024-04-19 06:52:00', '', null, '字典管理菜单');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (106, '参数设置', 1, 7, 'config', 'system/config/index', '', 1, 0, 'C', 0, 0, 'system:config:list', 'edit', 'admin', '2024-04-19 06:52:01', '', null, '参数设置菜单');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (107, '通知公告', 1, 8, 'notice', 'system/notice/index', '', 1, 0, 'C', 0, 1, 'system:notice:list', 'message', 'admin', '2024-04-19 06:52:02', 'admin', '2024-04-30 09:34:25', '通知公告菜单');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (108, '日志管理', 1, 9, 'log', '', '', 1, 0, 'M', 1, 1, '', 'log', 'admin', '2024-04-19 06:52:03', 'admin', '2024-04-30 09:34:18', '日志管理菜单');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (109, '在线用户', 2, 1, 'online', 'monitor/online/index', '', 1, 0, 'C', 0, 0, 'monitor:online:list', 'online', 'admin', '2024-04-19 06:52:03', '', null, '在线用户菜单');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (110, '定时任务', 2, 2, 'job', 'monitor/job/index', '', 1, 0, 'C', 0, 0, 'monitor:job:list', 'job', 'admin', '2024-04-19 06:52:04', '', null, '定时任务菜单');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (111, '数据监控', 2, 3, 'druid', 'monitor/druid/index', '', 1, 0, 'C', 0, 0, 'monitor:druid:list', 'druid', 'admin', '2024-04-19 06:52:05', '', null, '数据监控菜单');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (112, '服务监控', 2, 4, 'server', 'monitor/server/index', '', 1, 0, 'C', 0, 0, 'monitor:server:list', 'server', 'admin', '2024-04-19 06:52:05', '', null, '服务监控菜单');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (113, '缓存监控', 2, 5, 'cache', 'monitor/cache/index', '', 1, 0, 'C', 0, 0, 'monitor:cache:list', 'redis', 'admin', '2024-04-19 06:52:06', '', null, '缓存监控菜单');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (114, '缓存列表', 2, 6, 'cacheList', 'monitor/cache/list', '', 1, 0, 'C', 0, 0, 'monitor:cache:list', 'redis-list', 'admin', '2024-04-19 06:52:07', '', null, '缓存列表菜单');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (115, '表单构建', 3, 1, 'build', 'tool/build/index', '', 1, 0, 'C', 0, 0, 'tool:build:list', 'build', 'admin', '2024-04-19 06:52:08', '', null, '表单构建菜单');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (116, '代码生成', 3, 2, 'gen', 'tool/gen/index', '', 1, 0, 'C', 0, 0, 'tool:gen:list', 'code', 'admin', '2024-04-19 06:52:09', '', null, '代码生成菜单');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (117, '系统接口', 3, 3, 'swagger', 'tool/swagger/index', '', 1, 0, 'C', 0, 0, 'tool:swagger:list', 'swagger', 'admin', '2024-04-19 06:52:10', '', null, '系统接口菜单');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (500, '操作日志', 108, 1, 'operlog', 'monitor/operlog/index', '', 1, 0, 'C', 0, 0, 'monitor:operlog:list', 'form', 'admin', '2024-04-19 06:52:10', '', null, '操作日志菜单');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (501, '登录日志', 108, 2, 'logininfor', 'monitor/logininfor/index', '', 1, 0, 'C', 0, 0, 'monitor:logininfor:list', 'logininfor', 'admin', '2024-04-19 06:52:11', '', null, '登录日志菜单');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1000, '用户查询', 100, 1, '', '', '', 1, 0, 'F', 0, 0, 'system:user:query', '#', 'admin', '2024-04-19 06:52:12', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1001, '用户新增', 100, 2, '', '', '', 1, 0, 'F', 0, 0, 'system:user:add', '#', 'admin', '2024-04-19 06:52:13', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1002, '用户修改', 100, 3, '', '', '', 1, 0, 'F', 0, 0, 'system:user:edit', '#', 'admin', '2024-04-19 06:52:14', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1003, '用户删除', 100, 4, '', '', '', 1, 0, 'F', 0, 0, 'system:user:remove', '#', 'admin', '2024-04-19 06:52:14', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1004, '用户导出', 100, 5, '', '', '', 1, 0, 'F', 0, 0, 'system:user:export', '#', 'admin', '2024-04-19 06:52:15', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1005, '用户导入', 100, 6, '', '', '', 1, 0, 'F', 0, 0, 'system:user:import', '#', 'admin', '2024-04-19 06:52:16', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1006, '重置密码', 100, 7, '', '', '', 1, 0, 'F', 0, 0, 'system:user:resetPwd', '#', 'admin', '2024-04-19 06:52:17', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1007, '角色查询', 101, 1, '', '', '', 1, 0, 'F', 0, 0, 'system:role:query', '#', 'admin', '2024-04-19 06:52:18', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1008, '角色新增', 101, 2, '', '', '', 1, 0, 'F', 0, 0, 'system:role:add', '#', 'admin', '2024-04-19 06:52:19', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1009, '角色修改', 101, 3, '', '', '', 1, 0, 'F', 0, 0, 'system:role:edit', '#', 'admin', '2024-04-19 06:52:19', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1010, '角色删除', 101, 4, '', '', '', 1, 0, 'F', 0, 0, 'system:role:remove', '#', 'admin', '2024-04-19 06:52:20', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1011, '角色导出', 101, 5, '', '', '', 1, 0, 'F', 0, 0, 'system:role:export', '#', 'admin', '2024-04-19 06:52:21', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1012, '菜单查询', 102, 1, '', '', '', 1, 0, 'F', 0, 0, 'system:menu:query', '#', 'admin', '2024-04-19 06:52:22', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1013, '菜单新增', 102, 2, '', '', '', 1, 0, 'F', 0, 0, 'system:menu:add', '#', 'admin', '2024-04-19 06:52:22', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1014, '菜单修改', 102, 3, '', '', '', 1, 0, 'F', 0, 0, 'system:menu:edit', '#', 'admin', '2024-04-19 06:52:23', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1015, '菜单删除', 102, 4, '', '', '', 1, 0, 'F', 0, 0, 'system:menu:remove', '#', 'admin', '2024-04-19 06:52:24', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1016, '部门查询', 103, 1, '', '', '', 1, 0, 'F', 0, 0, 'system:dept:query', '#', 'admin', '2024-04-19 06:52:25', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1017, '部门新增', 103, 2, '', '', '', 1, 0, 'F', 0, 0, 'system:dept:add', '#', 'admin', '2024-04-19 06:52:26', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1018, '部门修改', 103, 3, '', '', '', 1, 0, 'F', 0, 0, 'system:dept:edit', '#', 'admin', '2024-04-19 06:52:27', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1019, '部门删除', 103, 4, '', '', '', 1, 0, 'F', 0, 0, 'system:dept:remove', '#', 'admin', '2024-04-19 06:52:27', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1020, '岗位查询', 104, 1, '', '', '', 1, 0, 'F', 0, 0, 'system:post:query', '#', 'admin', '2024-04-19 06:52:28', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1021, '岗位新增', 104, 2, '', '', '', 1, 0, 'F', 0, 0, 'system:post:add', '#', 'admin', '2024-04-19 06:52:29', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1022, '岗位修改', 104, 3, '', '', '', 1, 0, 'F', 0, 0, 'system:post:edit', '#', 'admin', '2024-04-19 06:52:30', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1023, '岗位删除', 104, 4, '', '', '', 1, 0, 'F', 0, 0, 'system:post:remove', '#', 'admin', '2024-04-19 06:52:31', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1024, '岗位导出', 104, 5, '', '', '', 1, 0, 'F', 0, 0, 'system:post:export', '#', 'admin', '2024-04-19 06:52:31', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1025, '字典查询', 105, 1, '#', '', '', 1, 0, 'F', 0, 0, 'system:dict:query', '#', 'admin', '2024-04-19 06:52:32', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1026, '字典新增', 105, 2, '#', '', '', 1, 0, 'F', 0, 0, 'system:dict:add', '#', 'admin', '2024-04-19 06:52:33', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1027, '字典修改', 105, 3, '#', '', '', 1, 0, 'F', 0, 0, 'system:dict:edit', '#', 'admin', '2024-04-19 06:52:34', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1028, '字典删除', 105, 4, '#', '', '', 1, 0, 'F', 0, 0, 'system:dict:remove', '#', 'admin', '2024-04-19 06:52:35', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1029, '字典导出', 105, 5, '#', '', '', 1, 0, 'F', 0, 0, 'system:dict:export', '#', 'admin', '2024-04-19 06:52:35', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1030, '参数查询', 106, 1, '#', '', '', 1, 0, 'F', 0, 0, 'system:config:query', '#', 'admin', '2024-04-19 06:52:36', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1031, '参数新增', 106, 2, '#', '', '', 1, 0, 'F', 0, 0, 'system:config:add', '#', 'admin', '2024-04-19 06:52:37', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1032, '参数修改', 106, 3, '#', '', '', 1, 0, 'F', 0, 0, 'system:config:edit', '#', 'admin', '2024-04-19 06:52:38', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1033, '参数删除', 106, 4, '#', '', '', 1, 0, 'F', 0, 0, 'system:config:remove', '#', 'admin', '2024-04-19 06:52:39', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1034, '参数导出', 106, 5, '#', '', '', 1, 0, 'F', 0, 0, 'system:config:export', '#', 'admin', '2024-04-19 06:52:39', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1035, '公告查询', 107, 1, '#', '', '', 1, 0, 'F', 0, 0, 'system:notice:query', '#', 'admin', '2024-04-19 06:52:40', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1036, '公告新增', 107, 2, '#', '', '', 1, 0, 'F', 0, 0, 'system:notice:add', '#', 'admin', '2024-04-19 06:52:41', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1037, '公告修改', 107, 3, '#', '', '', 1, 0, 'F', 0, 0, 'system:notice:edit', '#', 'admin', '2024-04-19 06:52:42', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1038, '公告删除', 107, 4, '#', '', '', 1, 0, 'F', 0, 0, 'system:notice:remove', '#', 'admin', '2024-04-19 06:52:43', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1039, '操作查询', 500, 1, '#', '', '', 1, 0, 'F', 0, 0, 'monitor:operlog:query', '#', 'admin', '2024-04-19 06:52:43', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1040, '操作删除', 500, 2, '#', '', '', 1, 0, 'F', 0, 0, 'monitor:operlog:remove', '#', 'admin', '2024-04-19 06:52:44', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1041, '日志导出', 500, 3, '#', '', '', 1, 0, 'F', 0, 0, 'monitor:operlog:export', '#', 'admin', '2024-04-19 06:52:45', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1042, '登录查询', 501, 1, '#', '', '', 1, 0, 'F', 0, 0, 'monitor:logininfor:query', '#', 'admin', '2024-04-19 06:52:46', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1043, '登录删除', 501, 2, '#', '', '', 1, 0, 'F', 0, 0, 'monitor:logininfor:remove', '#', 'admin', '2024-04-19 06:52:47', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1044, '日志导出', 501, 3, '#', '', '', 1, 0, 'F', 0, 0, 'monitor:logininfor:export', '#', 'admin', '2024-04-19 06:52:47', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1045, '账户解锁', 501, 4, '#', '', '', 1, 0, 'F', 0, 0, 'monitor:logininfor:unlock', '#', 'admin', '2024-04-19 06:52:48', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1046, '在线查询', 109, 1, '#', '', '', 1, 0, 'F', 0, 0, 'monitor:online:query', '#', 'admin', '2024-04-19 06:52:49', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1047, '批量强退', 109, 2, '#', '', '', 1, 0, 'F', 0, 0, 'monitor:online:batchLogout', '#', 'admin', '2024-04-19 06:52:50', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1048, '单条强退', 109, 3, '#', '', '', 1, 0, 'F', 0, 0, 'monitor:online:forceLogout', '#', 'admin', '2024-04-19 06:52:51', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1049, '任务查询', 110, 1, '#', '', '', 1, 0, 'F', 0, 0, 'monitor:job:query', '#', 'admin', '2024-04-19 06:52:51', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1050, '任务新增', 110, 2, '#', '', '', 1, 0, 'F', 0, 0, 'monitor:job:add', '#', 'admin', '2024-04-19 06:52:52', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1051, '任务修改', 110, 3, '#', '', '', 1, 0, 'F', 0, 0, 'monitor:job:edit', '#', 'admin', '2024-04-19 06:52:53', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1052, '任务删除', 110, 4, '#', '', '', 1, 0, 'F', 0, 0, 'monitor:job:remove', '#', 'admin', '2024-04-19 06:52:54', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1053, '状态修改', 110, 5, '#', '', '', 1, 0, 'F', 0, 0, 'monitor:job:changeStatus', '#', 'admin', '2024-04-19 06:52:55', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1054, '任务导出', 110, 6, '#', '', '', 1, 0, 'F', 0, 0, 'monitor:job:export', '#', 'admin', '2024-04-19 06:52:56', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1055, '生成查询', 116, 1, '#', '', '', 1, 0, 'F', 0, 0, 'tool:gen:query', '#', 'admin', '2024-04-19 06:52:56', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1056, '生成修改', 116, 2, '#', '', '', 1, 0, 'F', 0, 0, 'tool:gen:edit', '#', 'admin', '2024-04-19 06:52:57', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1057, '生成删除', 116, 3, '#', '', '', 1, 0, 'F', 0, 0, 'tool:gen:remove', '#', 'admin', '2024-04-19 06:52:58', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1058, '导入代码', 116, 4, '#', '', '', 1, 0, 'F', 0, 0, 'tool:gen:import', '#', 'admin', '2024-04-19 06:52:59', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1059, '预览代码', 116, 5, '#', '', '', 1, 0, 'F', 0, 0, 'tool:gen:preview', '#', 'admin', '2024-04-19 06:53:00', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (1060, '生成代码', 116, 6, '#', '', '', 1, 0, 'F', 0, 0, 'tool:gen:code', '#', 'admin', '2024-04-19 06:53:41', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2000, '客户管理', 0, 3, 'crm', null, null, 1, 0, 'M', 0, 0, '', 'peoples', 'admin', '2024-04-23 10:36:55', 'admin', '2024-04-23 10:37:16', '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2007, '产品分类', 2062, 1, 'category', 'crm/category/index', null, 1, 0, 'C', 0, 0, 'crm:category:list', 'clipboard', 'admin', '2024-04-25 16:02:38', 'admin', '2024-04-26 14:48:18', '产品分类菜单');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2008, '产品分类查询', 2007, 1, '#', '', null, 1, 0, 'F', 0, 0, 'crm:category:query', '#', 'admin', '2024-04-25 16:02:40', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2009, '产品分类新增', 2007, 2, '#', '', null, 1, 0, 'F', 0, 0, 'crm:category:add', '#', 'admin', '2024-04-25 16:02:41', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2010, '产品分类修改', 2007, 3, '#', '', null, 1, 0, 'F', 0, 0, 'crm:category:edit', '#', 'admin', '2024-04-25 16:02:42', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2011, '产品分类删除', 2007, 4, '#', '', null, 1, 0, 'F', 0, 0, 'crm:category:remove', '#', 'admin', '2024-04-25 16:02:42', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2012, '产品分类导出', 2007, 5, '#', '', null, 1, 0, 'F', 0, 0, 'crm:category:export', '#', 'admin', '2024-04-25 16:02:43', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2013, '联系人管理', 2000, 1, 'contacts', 'crm/contacts/index', null, 1, 0, 'C', 0, 0, 'crm:contacts:list', 'peoples', 'admin', '2024-04-25 16:02:55', 'admin', '2024-04-25 21:37:35', '联系人菜单');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2014, '联系人查询', 2013, 1, '#', '', null, 1, 0, 'F', 0, 0, 'crm:contacts:query', '#', 'admin', '2024-04-25 16:02:56', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2015, '联系人新增', 2013, 2, '#', '', null, 1, 0, 'F', 0, 0, 'crm:contacts:add', '#', 'admin', '2024-04-25 16:02:57', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2016, '联系人修改', 2013, 3, '#', '', null, 1, 0, 'F', 0, 0, 'crm:contacts:edit', '#', 'admin', '2024-04-25 16:02:58', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2017, '联系人删除', 2013, 4, '#', '', null, 1, 0, 'F', 0, 0, 'crm:contacts:remove', '#', 'admin', '2024-04-25 16:02:59', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2018, '联系人导出', 2013, 5, '#', '', null, 1, 0, 'F', 0, 0, 'crm:contacts:export', '#', 'admin', '2024-04-25 16:02:59', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2019, '合同管理', 2063, 1, 'contract', 'crm/contract/index', null, 1, 0, 'C', 0, 0, 'crm:contract:list', 'documentation', 'admin', '2024-04-25 16:03:04', 'admin', '2024-04-26 14:49:11', '合同菜单');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2020, '合同查询', 2019, 1, '#', '', null, 1, 0, 'F', 0, 0, 'crm:contract:query', '#', 'admin', '2024-04-25 16:03:06', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2021, '合同新增', 2019, 2, '#', '', null, 1, 0, 'F', 0, 0, 'crm:contract:add', '#', 'admin', '2024-04-25 16:03:06', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2022, '合同修改', 2019, 3, '#', '', null, 1, 0, 'F', 0, 0, 'crm:contract:edit', '#', 'admin', '2024-04-25 16:03:07', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2023, '合同删除', 2019, 4, '#', '', null, 1, 0, 'F', 0, 0, 'crm:contract:remove', '#', 'admin', '2024-04-25 16:03:08', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2024, '合同导出', 2019, 5, '#', '', null, 1, 0, 'F', 0, 0, 'crm:contract:export', '#', 'admin', '2024-04-25 16:03:09', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2025, '客户列表', 2000, 1, 'customer', 'crm/customer/index', null, 1, 0, 'C', 0, 0, 'crm:customer:list', 'logininfor', 'admin', '2024-04-25 16:03:13', 'admin', '2024-04-26 14:52:49', '客户菜单');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2026, '客户查询', 2025, 1, '#', '', null, 1, 0, 'F', 0, 0, 'crm:customer:query', '#', 'admin', '2024-04-25 16:03:15', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2027, '客户新增', 2025, 2, '#', '', null, 1, 0, 'F', 0, 0, 'crm:customer:add', '#', 'admin', '2024-04-25 16:03:15', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2028, '客户修改', 2025, 3, '#', '', null, 1, 0, 'F', 0, 0, 'crm:customer:edit', '#', 'admin', '2024-04-25 16:03:16', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2029, '客户删除', 2025, 4, '#', '', null, 1, 0, 'F', 0, 0, 'crm:customer:remove', '#', 'admin', '2024-04-25 16:03:17', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2030, '客户导出', 2025, 5, '#', '', null, 1, 0, 'F', 0, 0, 'crm:customer:export', '#', 'admin', '2024-04-25 16:03:18', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2031, '线索列表', 2061, 1, 'leads', 'crm/leads/index', null, 1, 0, 'C', 0, 0, 'crm:leads:list', 'log', 'admin', '2024-04-25 16:03:21', 'admin', '2024-04-26 14:53:26', '线索菜单');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2032, '线索查询', 2031, 1, '#', '', null, 1, 0, 'F', 0, 0, 'crm:leads:query', '#', 'admin', '2024-04-25 16:03:23', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2033, '线索新增', 2031, 2, '#', '', null, 1, 0, 'F', 0, 0, 'crm:leads:add', '#', 'admin', '2024-04-25 16:03:24', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2034, '线索修改', 2031, 3, '#', '', null, 1, 0, 'F', 0, 0, 'crm:leads:edit', '#', 'admin', '2024-04-25 16:03:25', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2035, '线索删除', 2031, 4, '#', '', null, 1, 0, 'F', 0, 0, 'crm:leads:remove', '#', 'admin', '2024-04-25 16:03:26', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2036, '线索导出', 2031, 5, '#', '', null, 1, 0, 'F', 0, 0, 'crm:leads:export', '#', 'admin', '2024-04-25 16:03:26', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2037, '回款计划', 2063, 1, 'plan', 'crm/plan/index', null, 1, 0, 'C', 0, 0, 'crm:plan:list', 'time', 'admin', '2024-04-25 16:03:31', 'admin', '2024-04-26 14:48:53', '回款计划菜单');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2038, '回款计划查询', 2037, 1, '#', '', null, 1, 0, 'F', 0, 0, 'crm:plan:query', '#', 'admin', '2024-04-25 16:03:33', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2039, '回款计划新增', 2037, 2, '#', '', null, 1, 0, 'F', 0, 0, 'crm:plan:add', '#', 'admin', '2024-04-25 16:03:34', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2040, '回款计划修改', 2037, 3, '#', '', null, 1, 0, 'F', 0, 0, 'crm:plan:edit', '#', 'admin', '2024-04-25 16:03:35', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2041, '回款计划删除', 2037, 4, '#', '', null, 1, 0, 'F', 0, 0, 'crm:plan:remove', '#', 'admin', '2024-04-25 16:03:35', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2042, '回款计划导出', 2037, 5, '#', '', null, 1, 0, 'F', 0, 0, 'crm:plan:export', '#', 'admin', '2024-04-25 16:03:36', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2043, '实际回款', 2063, 1, 'receivables', 'crm/receivables/index', null, 1, 0, 'C', 0, 0, 'crm:receivables:list', 'education', 'admin', '2024-04-25 16:03:42', 'admin', '2024-04-26 14:52:10', '回款菜单');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2044, '回款查询', 2043, 1, '#', '', null, 1, 0, 'F', 0, 0, 'crm:receivables:query', '#', 'admin', '2024-04-25 16:03:44', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2045, '回款新增', 2043, 2, '#', '', null, 1, 0, 'F', 0, 0, 'crm:receivables:add', '#', 'admin', '2024-04-25 16:03:45', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2046, '回款修改', 2043, 3, '#', '', null, 1, 0, 'F', 0, 0, 'crm:receivables:edit', '#', 'admin', '2024-04-25 16:03:45', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2047, '回款删除', 2043, 4, '#', '', null, 1, 0, 'F', 0, 0, 'crm:receivables:remove', '#', 'admin', '2024-04-25 16:03:46', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2048, '回款导出', 2043, 5, '#', '', null, 1, 0, 'F', 0, 0, 'crm:receivables:export', '#', 'admin', '2024-04-25 16:03:47', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2049, '产品列表', 2062, 1, 'product', 'crm/product/index', null, 1, 0, 'C', 0, 0, 'crm:product:list', 'form', 'admin', '2024-04-25 16:04:49', 'admin', '2024-04-26 14:52:34', '产品菜单');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2050, '产品查询', 2049, 1, '#', '', null, 1, 0, 'F', 0, 0, 'crm:product:query', '#', 'admin', '2024-04-25 16:04:51', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2051, '产品新增', 2049, 2, '#', '', null, 1, 0, 'F', 0, 0, 'crm:product:add', '#', 'admin', '2024-04-25 16:04:52', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2052, '产品修改', 2049, 3, '#', '', null, 1, 0, 'F', 0, 0, 'crm:product:edit', '#', 'admin', '2024-04-25 16:04:53', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2053, '产品删除', 2049, 4, '#', '', null, 1, 0, 'F', 0, 0, 'crm:product:remove', '#', 'admin', '2024-04-25 16:04:54', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2054, '产品导出', 2049, 5, '#', '', null, 1, 0, 'F', 0, 0, 'crm:product:export', '#', 'admin', '2024-04-25 16:04:55', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2055, '商机列表', 2064, 1, 'business', 'crm/business/index', null, 1, 0, 'C', 0, 0, 'crm:business:list', 'build', 'admin', '2024-04-25 16:05:28', 'admin', '2024-04-26 14:52:24', '商机菜单');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2056, '商机查询', 2055, 1, '#', '', null, 1, 0, 'F', 0, 0, 'crm:business:query', '#', 'admin', '2024-04-25 16:05:30', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2057, '商机新增', 2055, 2, '#', '', null, 1, 0, 'F', 0, 0, 'crm:business:add', '#', 'admin', '2024-04-25 16:05:31', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2058, '商机修改', 2055, 3, '#', '', null, 1, 0, 'F', 0, 0, 'crm:business:edit', '#', 'admin', '2024-04-25 16:05:32', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2059, '商机删除', 2055, 4, '#', '', null, 1, 0, 'F', 0, 0, 'crm:business:remove', '#', 'admin', '2024-04-25 16:05:32', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2060, '商机导出', 2055, 5, '#', '', null, 1, 0, 'F', 0, 0, 'crm:business:export', '#', 'admin', '2024-04-25 16:05:33', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2061, '线索管理', 0, 3, 'leads', null, null, 1, 0, 'M', 0, 0, null, 'dict', 'admin', '2024-04-26 14:43:29', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2062, '产品管理', 0, 4, 'product', null, null, 1, 0, 'M', 0, 0, null, 'education', 'admin', '2024-04-26 14:44:23', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2063, '财务管理', 0, 6, 'finance', null, null, 1, 0, 'M', 0, 0, null, 'druid', 'admin', '2024-04-26 14:45:33', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2064, '商机管理', 0, 5, 'business', null, null, 1, 0, 'M', 0, 0, null, 'example', 'admin', '2024-04-26 14:47:10', '', null, '');
insert into sys_menu (menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark) values (2065, '待办事项', 0, 0, 'wait', null, null, 1, 0, 'C', 0, 0, null, 'build', 'admin', '2024-04-26 14:51:05', '', null, '');


-- ----------------------------
-- 6、用户和角色关联表  用户N-1角色
-- ----------------------------
drop table if exists sys_user_role;
create table sys_user_role (
  user_id   bigint(20) not null comment '用户ID',
  role_id   bigint(20) not null comment '角色ID',
  primary key(user_id, role_id)
) engine=innodb comment = '用户和角色关联表';

-- ----------------------------
-- 初始化-用户和角色关联表数据
-- ----------------------------
insert into sys_user_role values ('1', '1');
insert into sys_user_role values ('2', '2');


-- ----------------------------
-- 7、角色和菜单关联表  角色1-N菜单
-- ----------------------------
drop table if exists sys_role_menu;
create table sys_role_menu (
  role_id   bigint(20) not null comment '角色ID',
  menu_id   bigint(20) not null comment '菜单ID',
  primary key(role_id, menu_id)
) engine=innodb comment = '角色和菜单关联表';

-- ----------------------------
-- 初始化-角色和菜单关联表数据
-- ----------------------------
insert into sys_role_menu values ('2', '1');
insert into sys_role_menu values ('2', '2');
insert into sys_role_menu values ('2', '3');
insert into sys_role_menu values ('2', '4');
insert into sys_role_menu values ('2', '100');
insert into sys_role_menu values ('2', '101');
insert into sys_role_menu values ('2', '102');
insert into sys_role_menu values ('2', '103');
insert into sys_role_menu values ('2', '104');
insert into sys_role_menu values ('2', '105');
insert into sys_role_menu values ('2', '106');
insert into sys_role_menu values ('2', '107');
insert into sys_role_menu values ('2', '108');
insert into sys_role_menu values ('2', '109');
insert into sys_role_menu values ('2', '110');
insert into sys_role_menu values ('2', '111');
insert into sys_role_menu values ('2', '112');
insert into sys_role_menu values ('2', '113');
insert into sys_role_menu values ('2', '114');
insert into sys_role_menu values ('2', '115');
insert into sys_role_menu values ('2', '116');
insert into sys_role_menu values ('2', '117');
insert into sys_role_menu values ('2', '500');
insert into sys_role_menu values ('2', '501');
insert into sys_role_menu values ('2', '1000');
insert into sys_role_menu values ('2', '1001');
insert into sys_role_menu values ('2', '1002');
insert into sys_role_menu values ('2', '1003');
insert into sys_role_menu values ('2', '1004');
insert into sys_role_menu values ('2', '1005');
insert into sys_role_menu values ('2', '1006');
insert into sys_role_menu values ('2', '1007');
insert into sys_role_menu values ('2', '1008');
insert into sys_role_menu values ('2', '1009');
insert into sys_role_menu values ('2', '1010');
insert into sys_role_menu values ('2', '1011');
insert into sys_role_menu values ('2', '1012');
insert into sys_role_menu values ('2', '1013');
insert into sys_role_menu values ('2', '1014');
insert into sys_role_menu values ('2', '1015');
insert into sys_role_menu values ('2', '1016');
insert into sys_role_menu values ('2', '1017');
insert into sys_role_menu values ('2', '1018');
insert into sys_role_menu values ('2', '1019');
insert into sys_role_menu values ('2', '1020');
insert into sys_role_menu values ('2', '1021');
insert into sys_role_menu values ('2', '1022');
insert into sys_role_menu values ('2', '1023');
insert into sys_role_menu values ('2', '1024');
insert into sys_role_menu values ('2', '1025');
insert into sys_role_menu values ('2', '1026');
insert into sys_role_menu values ('2', '1027');
insert into sys_role_menu values ('2', '1028');
insert into sys_role_menu values ('2', '1029');
insert into sys_role_menu values ('2', '1030');
insert into sys_role_menu values ('2', '1031');
insert into sys_role_menu values ('2', '1032');
insert into sys_role_menu values ('2', '1033');
insert into sys_role_menu values ('2', '1034');
insert into sys_role_menu values ('2', '1035');
insert into sys_role_menu values ('2', '1036');
insert into sys_role_menu values ('2', '1037');
insert into sys_role_menu values ('2', '1038');
insert into sys_role_menu values ('2', '1039');
insert into sys_role_menu values ('2', '1040');
insert into sys_role_menu values ('2', '1041');
insert into sys_role_menu values ('2', '1042');
insert into sys_role_menu values ('2', '1043');
insert into sys_role_menu values ('2', '1044');
insert into sys_role_menu values ('2', '1045');
insert into sys_role_menu values ('2', '1046');
insert into sys_role_menu values ('2', '1047');
insert into sys_role_menu values ('2', '1048');
insert into sys_role_menu values ('2', '1049');
insert into sys_role_menu values ('2', '1050');
insert into sys_role_menu values ('2', '1051');
insert into sys_role_menu values ('2', '1052');
insert into sys_role_menu values ('2', '1053');
insert into sys_role_menu values ('2', '1054');
insert into sys_role_menu values ('2', '1055');
insert into sys_role_menu values ('2', '1056');
insert into sys_role_menu values ('2', '1057');
insert into sys_role_menu values ('2', '1058');
insert into sys_role_menu values ('2', '1059');
insert into sys_role_menu values ('2', '1060');

-- ----------------------------
-- 8、角色和部门关联表  角色1-N部门
-- ----------------------------
drop table if exists sys_role_dept;
create table sys_role_dept (
  role_id   bigint(20) not null comment '角色ID',
  dept_id   bigint(20) not null comment '部门ID',
  primary key(role_id, dept_id)
) engine=innodb comment = '角色和部门关联表';

-- ----------------------------
-- 初始化-角色和部门关联表数据
-- ----------------------------
insert into sys_role_dept values ('2', '100');
insert into sys_role_dept values ('2', '101');
insert into sys_role_dept values ('2', '105');


-- ----------------------------
-- 9、用户与岗位关联表  用户1-N岗位
-- ----------------------------
drop table if exists sys_user_post;
create table sys_user_post
(
  user_id   bigint(20) not null comment '用户ID',
  post_id   bigint(20) not null comment '岗位ID',
  primary key (user_id, post_id)
) engine=innodb comment = '用户与岗位关联表';

-- ----------------------------
-- 初始化-用户与岗位关联表数据
-- ----------------------------
insert into sys_user_post values ('1', '1');
insert into sys_user_post values ('2', '2');


-- ----------------------------
-- 10、操作日志记录
-- ----------------------------
drop table if exists sys_oper_log;
create table sys_oper_log (
  oper_id           bigint(20)      not null auto_increment    comment '日志主键',
  title             varchar(50)     default ''                 comment '模块标题',
  business_type     int(2)          default 0                  comment '业务类型（0其它 1新增 2修改 3删除）',
  method            varchar(100)    default ''                 comment '方法名称',
  request_method    varchar(10)     default ''                 comment '请求方式',
  operator_type     int(1)          default 0                  comment '操作类别（0其它 1后台用户 2手机端用户）',
  oper_name         varchar(50)     default ''                 comment '操作人员',
  dept_name         varchar(50)     default ''                 comment '部门名称',
  oper_url          varchar(255)    default ''                 comment '请求URL',
  oper_ip           varchar(128)    default ''                 comment '主机地址',
  oper_location     varchar(255)    default ''                 comment '操作地点',
  oper_param        varchar(2000)   default ''                 comment '请求参数',
  json_result       varchar(2000)   default ''                 comment '返回参数',
  status            int(1)          default 0                  comment '操作状态（0正常 1异常）',
  error_msg         varchar(2000)   default ''                 comment '错误消息',
  oper_time         datetime                                   comment '操作时间',
  cost_time         bigint(20)      default 0                  comment '消耗时间',
  primary key (oper_id),
  key idx_sys_oper_log_bt (business_type),
  key idx_sys_oper_log_s  (status),
  key idx_sys_oper_log_ot (oper_time)
) engine=innodb auto_increment=100 comment = '操作日志记录';


-- ----------------------------
-- 11、字典类型表
-- ----------------------------
drop table if exists sys_dict_type;
create table sys_dict_type
(
  dict_id          bigint(20)      not null auto_increment    comment '字典主键',
  dict_name        varchar(100)    default ''                 comment '字典名称',
  dict_type        varchar(100)    default ''                 comment '字典类型',
  status           char(1)         default '0'                comment '状态（0正常 1停用）',
  create_by        varchar(64)     default ''                 comment '创建者',
  create_time      datetime                                   comment '创建时间',
  update_by        varchar(64)     default ''                 comment '更新者',
  update_time      datetime                                   comment '更新时间',
  remark           varchar(500)    default null               comment '备注',
  primary key (dict_id),
  unique (dict_type)
) engine=innodb auto_increment=100 comment = '字典类型表';

insert into sys_dict_type (dict_id, dict_name, dict_type, status, create_by, create_time, update_by, update_time, remark) values (1, '用户性别', 'sys_user_sex', 0, 'admin', '2024-04-14 20:12:24', '', null, '用户性别列表');
insert into sys_dict_type (dict_id, dict_name, dict_type, status, create_by, create_time, update_by, update_time, remark) values (2, '菜单状态', 'sys_show_hide', 0, 'admin', '2024-04-14 20:12:24', '', null, '菜单状态列表');
insert into sys_dict_type (dict_id, dict_name, dict_type, status, create_by, create_time, update_by, update_time, remark) values (3, '系统开关', 'sys_normal_disable', 0, 'admin', '2024-04-14 20:12:25', '', null, '系统开关列表');
insert into sys_dict_type (dict_id, dict_name, dict_type, status, create_by, create_time, update_by, update_time, remark) values (4, '任务状态', 'sys_job_status', 0, 'admin', '2024-04-14 20:12:26', '', null, '任务状态列表');
insert into sys_dict_type (dict_id, dict_name, dict_type, status, create_by, create_time, update_by, update_time, remark) values (5, '任务分组', 'sys_job_group', 0, 'admin', '2024-04-14 20:12:27', '', null, '任务分组列表');
insert into sys_dict_type (dict_id, dict_name, dict_type, status, create_by, create_time, update_by, update_time, remark) values (6, '系统是否', 'sys_yes_no', 0, 'admin', '2024-04-14 20:12:28', '', null, '系统是否列表');
insert into sys_dict_type (dict_id, dict_name, dict_type, status, create_by, create_time, update_by, update_time, remark) values (7, '通知类型', 'sys_notice_type', 0, 'admin', '2024-04-14 20:12:29', '', null, '通知类型列表');
insert into sys_dict_type (dict_id, dict_name, dict_type, status, create_by, create_time, update_by, update_time, remark) values (8, '通知状态', 'sys_notice_status', 0, 'admin', '2024-04-14 20:12:30', '', null, '通知状态列表');
insert into sys_dict_type (dict_id, dict_name, dict_type, status, create_by, create_time, update_by, update_time, remark) values (9, '操作类型', 'sys_oper_type', 0, 'admin', '2024-04-14 20:12:31', '', null, '操作类型列表');
insert into sys_dict_type (dict_id, dict_name, dict_type, status, create_by, create_time, update_by, update_time, remark) values (10, '系统状态', 'sys_common_status', 0, 'admin', '2024-04-14 20:12:31', '', null, '登录状态列表');
insert into sys_dict_type (dict_id, dict_name, dict_type, status, create_by, create_time, update_by, update_time, remark) values (100, '客户性质', 'customer_nature', 0, 'admin', '2024-05-01 19:01:47', '', null, null);
insert into sys_dict_type (dict_id, dict_name, dict_type, status, create_by, create_time, update_by, update_time, remark) values (101, '客户来源', 'customer_source', 0, 'admin', '2024-05-01 19:22:01', '', null, null);
insert into sys_dict_type (dict_id, dict_name, dict_type, status, create_by, create_time, update_by, update_time, remark) values (102, '客户行业', 'customer_industry', 0, 'admin', '2024-05-01 19:24:15', '', null, null);
insert into sys_dict_type (dict_id, dict_name, dict_type, status, create_by, create_time, update_by, update_time, remark) values (103, '客户分级', 'customer_grade', 0, 'admin', '2024-05-01 19:27:36', '', null, null);
insert into sys_dict_type (dict_id, dict_name, dict_type, status, create_by, create_time, update_by, update_time, remark) values (104, '客户阶段', 'customer_stage', 0, 'admin', '2024-05-01 19:27:55', '', null, null);
insert into sys_dict_type (dict_id, dict_name, dict_type, status, create_by, create_time, update_by, update_time, remark) values (105, '客户类型', 'customer_type', 0, 'admin', '2024-05-01 19:30:08', '', null, null);
insert into sys_dict_type (dict_id, dict_name, dict_type, status, create_by, create_time, update_by, update_time, remark) values (106, '跟进类型', 'record_category', 0, 'admin', '2024-05-06 20:47:22', '', null, null);


-- ----------------------------
-- 12、字典数据表
-- ----------------------------
drop table if exists sys_dict_data;
create table sys_dict_data
(
  dict_code        bigint(20)      not null auto_increment    comment '字典编码',
  dict_sort        int(4)          default 0                  comment '字典排序',
  dict_label       varchar(100)    default ''                 comment '字典标签',
  dict_value       varchar(100)    default ''                 comment '字典键值',
  dict_type        varchar(100)    default ''                 comment '字典类型',
  css_class        varchar(100)    default null               comment '样式属性（其他样式扩展）',
  list_class       varchar(100)    default null               comment '表格回显样式',
  is_default       char(1)         default 'N'                comment '是否默认（Y是 N否）',
  status           char(1)         default '0'                comment '状态（0正常 1停用）',
  create_by        varchar(64)     default ''                 comment '创建者',
  create_time      datetime                                   comment '创建时间',
  update_by        varchar(64)     default ''                 comment '更新者',
  update_time      datetime                                   comment '更新时间',
  remark           varchar(500)    default null               comment '备注',
  primary key (dict_code)
) engine=innodb auto_increment=100 comment = '字典数据表';

insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (1, 1, '男', 0, 'sys_user_sex', '', '', 'Y', 0, 'admin', '2024-04-14 20:12:34', '', null, '性别男');
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (2, 2, '女', 1, 'sys_user_sex', '', '', 'N', 0, 'admin', '2024-04-14 20:12:35', '', null, '性别女');
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (3, 3, '未知', 2, 'sys_user_sex', '', '', 'N', 0, 'admin', '2024-04-14 20:12:36', '', null, '性别未知');
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (4, 1, '显示', 0, 'sys_show_hide', '', 'primary', 'Y', 0, 'admin', '2024-04-14 20:12:37', '', null, '显示菜单');
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (5, 2, '隐藏', 1, 'sys_show_hide', '', 'danger', 'N', 0, 'admin', '2024-04-14 20:12:37', '', null, '隐藏菜单');
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (6, 1, '正常', 0, 'sys_normal_disable', '', 'primary', 'Y', 0, 'admin', '2024-04-14 20:12:39', '', null, '正常状态');
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (7, 2, '停用', 1, 'sys_normal_disable', '', 'danger', 'N', 0, 'admin', '2024-04-14 20:12:40', '', null, '停用状态');
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (8, 1, '正常', 0, 'sys_job_status', '', 'primary', 'Y', 0, 'admin', '2024-04-14 20:12:41', '', null, '正常状态');
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (9, 2, '暂停', 1, 'sys_job_status', '', 'danger', 'N', 0, 'admin', '2024-04-14 20:12:42', '', null, '停用状态');
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', 0, 'admin', '2024-04-14 20:12:42', '', null, '默认分组');
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', 0, 'admin', '2024-04-14 20:12:43', '', null, '系统分组');
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', 0, 'admin', '2024-04-14 20:12:44', '', null, '系统默认是');
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', 0, 'admin', '2024-04-14 20:12:45', '', null, '系统默认否');
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (14, 1, '通知', 1, 'sys_notice_type', '', 'warning', 'Y', 0, 'admin', '2024-04-14 20:12:46', '', null, '通知');
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (15, 2, '公告', 2, 'sys_notice_type', '', 'success', 'N', 0, 'admin', '2024-04-14 20:12:47', '', null, '公告');
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (16, 1, '正常', 0, 'sys_notice_status', '', 'primary', 'Y', 0, 'admin', '2024-04-14 20:12:48', '', null, '正常状态');
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (17, 2, '关闭', 1, 'sys_notice_status', '', 'danger', 'N', 0, 'admin', '2024-04-14 20:12:48', '', null, '关闭状态');
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (18, 99, '其他', 0, 'sys_oper_type', '', 'info', 'N', 0, 'admin', '2024-04-14 20:12:49', '', null, '其他操作');
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (19, 1, '新增', 1, 'sys_oper_type', '', 'info', 'N', 0, 'admin', '2024-04-14 20:12:50', '', null, '新增操作');
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (20, 2, '修改', 2, 'sys_oper_type', '', 'info', 'N', 0, 'admin', '2024-04-14 20:12:51', '', null, '修改操作');
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (21, 3, '删除', 3, 'sys_oper_type', '', 'danger', 'N', 0, 'admin', '2024-04-14 20:12:51', '', null, '删除操作');
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (22, 4, '授权', 4, 'sys_oper_type', '', 'primary', 'N', 0, 'admin', '2024-04-14 20:12:52', '', null, '授权操作');
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (23, 5, '导出', 5, 'sys_oper_type', '', 'warning', 'N', 0, 'admin', '2024-04-14 20:12:53', '', null, '导出操作');
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (24, 6, '导入', 6, 'sys_oper_type', '', 'warning', 'N', 0, 'admin', '2024-04-14 20:12:54', '', null, '导入操作');
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (25, 7, '强退', 7, 'sys_oper_type', '', 'danger', 'N', 0, 'admin', '2024-04-14 20:12:55', '', null, '强退操作');
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (26, 8, '生成代码', 8, 'sys_oper_type', '', 'warning', 'N', 0, 'admin', '2024-04-14 20:12:55', '', null, '生成操作');
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (27, 9, '清空数据', 9, 'sys_oper_type', '', 'danger', 'N', 0, 'admin', '2024-04-14 20:12:56', '', null, '清空操作');
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (28, 1, '成功', 0, 'sys_common_status', '', 'primary', 'N', 0, 'admin', '2024-04-14 20:12:57', '', null, '正常状态');
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (29, 2, '失败', 1, 'sys_common_status', '', 'danger', 'N', 0, 'admin', '2024-04-14 20:12:58', '', null, '停用状态');
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (100, 1, '企业客户', 1, 'customer_nature', '', 'default', 'N', 0, 'admin', '2024-05-01 19:02:26', 'admin', '2024-05-01 19:03:34', null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (101, 2, '个人客户', 2, 'customer_nature', null, 'default', 'N', 0, 'admin', '2024-05-01 19:03:28', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (102, 3, '政府单位', 3, 'customer_nature', null, 'default', 'N', 0, 'admin', '2024-05-01 19:03:56', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (103, 4, '事业单位', 4, 'customer_nature', null, 'default', 'N', 0, 'admin', '2024-05-01 19:04:13', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (104, 1, '电话咨询', 1, 'customer_source', null, 'default', 'N', 0, 'admin', '2024-05-01 19:22:17', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (105, 2, '自主开拓', 2, 'customer_source', null, 'default', 'N', 0, 'admin', '2024-05-01 19:22:26', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (106, 3, '公司资源', 3, 'customer_source', null, 'default', 'N', 0, 'admin', '2024-05-01 19:22:34', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (107, 4, '网络推广', 4, 'customer_source', null, 'default', 'N', 0, 'admin', '2024-05-01 19:22:44', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (108, 5, '渠道代理', 5, 'customer_source', null, 'default', 'N', 0, 'admin', '2024-05-01 19:22:54', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (109, 6, '其他方式', 6, 'customer_source', null, 'default', 'N', 0, 'admin', '2024-05-01 19:23:03', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (110, 1, '金融', 1, 'customer_industry', null, 'default', 'N', 0, 'admin', '2024-05-01 19:24:29', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (111, 2, '电信', 2, 'customer_industry', null, 'default', 'N', 0, 'admin', '2024-05-01 19:24:51', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (112, 3, '教育', 3, 'customer_industry', null, 'default', 'N', 0, 'admin', '2024-05-01 19:24:59', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (113, 4, '高科技', 4, 'customer_industry', null, 'default', 'N', 0, 'admin', '2024-05-01 19:25:09', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (114, 5, '政府', 5, 'customer_industry', null, 'default', 'N', 0, 'admin', '2024-05-01 19:25:18', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (115, 6, '制造业', 6, 'customer_industry', null, 'default', 'N', 0, 'admin', '2024-05-01 19:25:27', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (116, 7, '服务业', 7, 'customer_industry', null, 'default', 'N', 0, 'admin', '2024-05-01 19:25:35', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (117, 8, '能源', 8, 'customer_industry', null, 'default', 'N', 0, 'admin', '2024-05-01 19:25:48', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (118, 9, '零售', 9, 'customer_industry', null, 'default', 'N', 0, 'admin', '2024-05-01 19:25:57', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (119, 10, '媒体', 10, 'customer_industry', null, 'default', 'N', 0, 'admin', '2024-05-01 19:26:06', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (120, 11, '娱乐', 11, 'customer_industry', null, 'default', 'N', 0, 'admin', '2024-05-01 19:26:15', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (121, 12, '咨询', 12, 'customer_industry', null, 'default', 'N', 0, 'admin', '2024-05-01 19:26:25', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (122, 13, '非盈利事业', 13, 'customer_industry', null, 'default', 'N', 0, 'admin', '2024-05-01 19:26:33', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (123, 14, '公用事业', 14, 'customer_industry', null, 'default', 'N', 0, 'admin', '2024-05-01 19:26:42', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (124, 15, '其他', 15, 'customer_industry', null, 'default', 'N', 0, 'admin', '2024-05-01 19:26:51', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (125, 1, '大型', 1, 'customer_grade', null, 'default', 'N', 0, 'admin', '2024-05-01 19:28:24', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (126, 2, '中型', 2, 'customer_grade', null, 'default', 'N', 0, 'admin', '2024-05-01 19:28:32', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (127, 3, '小型', 3, 'customer_grade', null, 'default', 'N', 0, 'admin', '2024-05-01 19:28:40', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (128, 4, '微型', 4, 'customer_grade', null, 'default', 'N', 0, 'admin', '2024-05-01 19:28:48', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (129, 1, '初步接洽', 1, 'customer_stage', null, 'default', 'N', 0, 'admin', '2024-05-01 19:29:04', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (130, 2, '确认需求', 2, 'customer_stage', null, 'default', 'N', 0, 'admin', '2024-05-01 19:29:11', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (131, 3, '解决方案', 3, 'customer_stage', null, 'default', 'N', 0, 'admin', '2024-05-01 19:29:19', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (132, 4, '商务谈判', 4, 'customer_stage', null, 'default', 'N', 0, 'admin', '2024-05-01 19:29:27', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (133, 5, '成交', 5, 'customer_stage', null, 'default', 'N', 0, 'admin', '2024-05-01 19:29:35', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (134, 6, '流失', 6, 'customer_stage', null, 'default', 'N', 0, 'admin', '2024-05-01 19:29:43', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (135, 1, '最终客户', 1, 'customer_type', null, 'default', 'N', 0, 'admin', '2024-05-01 19:30:19', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (136, 2, '渠道客户', 2, 'customer_type', null, 'default', 'N', 0, 'admin', '2024-05-01 19:30:25', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (137, 3, '竞争对手', 3, 'customer_type', null, 'default', 'N', 0, 'admin', '2024-05-01 19:30:33', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (138, 1, '外出拜访', 1, 'record_category', null, 'default', 'N', 0, 'admin', '2024-05-06 20:47:41', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (139, 2, '客户面谈', 2, 'record_category', null, 'default', 'N', 0, 'admin', '2024-05-06 20:47:52', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (140, 3, '客户来电', 3, 'record_category', null, 'default', 'N', 0, 'admin', '2024-05-06 20:48:04', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (141, 4, '客户来访', 4, 'record_category', null, 'default', 'N', 0, 'admin', '2024-05-06 20:48:13', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (142, 5, '在线交流', 5, 'record_category', null, 'default', 'N', 0, 'admin', '2024-05-06 20:48:21', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (143, 6, '发送资料', 6, 'record_category', null, 'default', 'N', 0, 'admin', '2024-05-06 20:48:21', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (144, 7, '售后回访', 7, 'record_category', null, 'default', 'N', 0, 'admin', '2024-05-06 20:48:21', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (145, 8, '方案整理', 8, 'record_category', null, 'default', 'N', 0, 'admin', '2024-05-06 20:48:21', '', null, null);
insert into sys_dict_data (dict_code, dict_sort, dict_label, dict_value, dict_type, css_class, list_class, is_default, status, create_by, create_time, update_by, update_time, remark) values (155, 9, '其他方面', 9, 'record_category', null, 'default', 'N', 0, 'admin', '2024-05-06 20:48:21', '', null, null);


-- ----------------------------
-- 13、参数配置表
-- ----------------------------
drop table if exists sys_config;
create table sys_config (
  config_id         int(5)          not null auto_increment    comment '参数主键',
  config_name       varchar(100)    default ''                 comment '参数名称',
  config_key        varchar(100)    default ''                 comment '参数键名',
  config_value      varchar(500)    default ''                 comment '参数键值',
  config_type       char(1)         default 'N'                comment '系统内置（Y是 N否）',
  create_by         varchar(64)     default ''                 comment '创建者',
  create_time       datetime                                   comment '创建时间',
  update_by         varchar(64)     default ''                 comment '更新者',
  update_time       datetime                                   comment '更新时间',
  remark            varchar(500)    default null               comment '备注',
  primary key (config_id)
) engine=innodb auto_increment=100 comment = '参数配置表';

insert into sys_config values(1, '主框架页-默认皮肤样式名称',     'sys.index.skinName',            'skin-blue',     'Y', 'admin', sysdate(), '', null, '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow' );
insert into sys_config values(2, '用户管理-账号初始密码',         'sys.user.initPassword',         '123456',        'Y', 'admin', sysdate(), '', null, '初始化密码 123456' );
insert into sys_config values(3, '主框架页-侧边栏主题',           'sys.index.sideTheme',           'theme-dark',    'Y', 'admin', sysdate(), '', null, '深色主题theme-dark，浅色主题theme-light' );
insert into sys_config values(4, '账号自助-验证码开关',           'sys.account.captchaEnabled',    'true',          'Y', 'admin', sysdate(), '', null, '是否开启验证码功能（true开启，false关闭）');
insert into sys_config values(5, '账号自助-是否开启用户注册功能', 'sys.account.registerUser',      'false',         'Y', 'admin', sysdate(), '', null, '是否开启注册用户功能（true开启，false关闭）');
insert into sys_config values(6, '用户登录-黑名单列表',           'sys.login.blackIPList',         '',              'Y', 'admin', sysdate(), '', null, '设置登录IP黑名单限制，多个匹配项以;分隔，支持匹配（*通配、网段）');


-- ----------------------------
-- 14、系统访问记录
-- ----------------------------
drop table if exists sys_logininfor;
create table sys_logininfor (
  info_id        bigint(20)     not null auto_increment   comment '访问ID',
  user_name      varchar(50)    default ''                comment '用户账号',
  ipaddr         varchar(128)   default ''                comment '登录IP地址',
  login_location varchar(255)   default ''                comment '登录地点',
  browser        varchar(50)    default ''                comment '浏览器类型',
  os             varchar(50)    default ''                comment '操作系统',
  status         char(1)        default '0'               comment '登录状态（0成功 1失败）',
  msg            varchar(255)   default ''                comment '提示消息',
  login_time     datetime                                 comment '访问时间',
  primary key (info_id),
  key idx_sys_logininfor_s  (status),
  key idx_sys_logininfor_lt (login_time)
) engine=innodb auto_increment=100 comment = '系统访问记录';


-- ----------------------------
-- 15、定时任务调度表
-- ----------------------------
drop table if exists sys_job;
create table sys_job (
  job_id              bigint(20)    not null auto_increment    comment '任务ID',
  job_name            varchar(64)   default ''                 comment '任务名称',
  job_group           varchar(64)   default 'DEFAULT'          comment '任务组名',
  invoke_target       varchar(500)  not null                   comment '调用目标字符串',
  cron_expression     varchar(255)  default ''                 comment 'cron执行表达式',
  misfire_policy      varchar(20)   default '3'                comment '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  concurrent          char(1)       default '1'                comment '是否并发执行（0允许 1禁止）',
  status              char(1)       default '0'                comment '状态（0正常 1暂停）',
  create_by           varchar(64)   default ''                 comment '创建者',
  create_time         datetime                                 comment '创建时间',
  update_by           varchar(64)   default ''                 comment '更新者',
  update_time         datetime                                 comment '更新时间',
  remark              varchar(500)  default ''                 comment '备注信息',
  primary key (job_id, job_name, job_group)
) engine=innodb auto_increment=100 comment = '定时任务调度表';

insert into sys_job values(1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams',        '0/10 * * * * ?', '3', '1', '1', 'admin', sysdate(), '', null, '');
insert into sys_job values(2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')',  '0/15 * * * * ?', '3', '1', '1', 'admin', sysdate(), '', null, '');
insert into sys_job values(3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)',  '0/20 * * * * ?', '3', '1', '1', 'admin', sysdate(), '', null, '');


-- ----------------------------
-- 16、定时任务调度日志表
-- ----------------------------
drop table if exists sys_job_log;
create table sys_job_log (
  job_log_id          bigint(20)     not null auto_increment    comment '任务日志ID',
  job_name            varchar(64)    not null                   comment '任务名称',
  job_group           varchar(64)    not null                   comment '任务组名',
  invoke_target       varchar(500)   not null                   comment '调用目标字符串',
  job_message         varchar(500)                              comment '日志信息',
  status              char(1)        default '0'                comment '执行状态（0正常 1失败）',
  exception_info      varchar(2000)  default ''                 comment '异常信息',
  create_time         datetime                                  comment '创建时间',
  primary key (job_log_id)
) engine=innodb comment = '定时任务调度日志表';


-- ----------------------------
-- 17、通知公告表
-- ----------------------------
drop table if exists sys_notice;
create table sys_notice (
  notice_id         int(4)          not null auto_increment    comment '公告ID',
  notice_title      varchar(50)     not null                   comment '公告标题',
  notice_type       char(1)         not null                   comment '公告类型（1通知 2公告）',
  notice_content    longblob        default null               comment '公告内容',
  status            char(1)         default '0'                comment '公告状态（0正常 1关闭）',
  create_by         varchar(64)     default ''                 comment '创建者',
  create_time       datetime                                   comment '创建时间',
  update_by         varchar(64)     default ''                 comment '更新者',
  update_time       datetime                                   comment '更新时间',
  remark            varchar(255)    default null               comment '备注',
  primary key (notice_id)
) engine=innodb auto_increment=10 comment = '通知公告表';

-- ----------------------------
-- 初始化-公告信息表数据
-- ----------------------------
insert into sys_notice values('1', '温馨提醒：2018-07-01 若依新版本发布啦', '2', '新版本内容', '0', 'admin', sysdate(), '', null, '管理员');
insert into sys_notice values('2', '维护通知：2018-07-01 若依系统凌晨维护', '1', '维护内容',   '0', 'admin', sysdate(), '', null, '管理员');


-- ----------------------------
-- 18、代码生成业务表
-- ----------------------------
drop table if exists gen_table;
create table gen_table (
  table_id          bigint(20)      not null auto_increment    comment '编号',
  table_name        varchar(200)    default ''                 comment '表名称',
  table_comment     varchar(500)    default ''                 comment '表描述',
  sub_table_name    varchar(64)     default null               comment '关联子表的表名',
  sub_table_fk_name varchar(64)     default null               comment '子表关联的外键名',
  class_name        varchar(100)    default ''                 comment '实体类名称',
  tpl_category      varchar(200)    default 'crud'             comment '使用的模板（crud单表操作 tree树表操作）',
  tpl_web_type      varchar(30)     default ''                 comment '前端模板类型（element-ui模版 element-plus模版）',
  package_name      varchar(100)                               comment '生成包路径',
  module_name       varchar(30)                                comment '生成模块名',
  business_name     varchar(30)                                comment '生成业务名',
  function_name     varchar(50)                                comment '生成功能名',
  function_author   varchar(50)                                comment '生成功能作者',
  gen_type          char(1)         default '0'                comment '生成代码方式（0zip压缩包 1自定义路径）',
  gen_path          varchar(200)    default '/'                comment '生成路径（不填默认项目路径）',
  options           varchar(1000)                              comment '其它生成选项',
  create_by         varchar(64)     default ''                 comment '创建者',
  create_time 	    datetime                                   comment '创建时间',
  update_by         varchar(64)     default ''                 comment '更新者',
  update_time       datetime                                   comment '更新时间',
  remark            varchar(500)    default null               comment '备注',
  primary key (table_id)
) engine=innodb auto_increment=1 comment = '代码生成业务表';


-- ----------------------------
-- 19、代码生成业务表字段
-- ----------------------------
drop table if exists gen_table_column;
create table gen_table_column (
  column_id         bigint(20)      not null auto_increment    comment '编号',
  table_id          bigint(20)                                 comment '归属表编号',
  column_name       varchar(200)                               comment '列名称',
  column_comment    varchar(500)                               comment '列描述',
  column_type       varchar(100)                               comment '列类型',
  java_type         varchar(500)                               comment 'JAVA类型',
  java_field        varchar(200)                               comment 'JAVA字段名',
  is_pk             char(1)                                    comment '是否主键（1是）',
  is_increment      char(1)                                    comment '是否自增（1是）',
  is_required       char(1)                                    comment '是否必填（1是）',
  is_insert         char(1)                                    comment '是否为插入字段（1是）',
  is_edit           char(1)                                    comment '是否编辑字段（1是）',
  is_list           char(1)                                    comment '是否列表字段（1是）',
  is_query          char(1)                                    comment '是否查询字段（1是）',
  query_type        varchar(200)    default 'EQ'               comment '查询方式（等于、不等于、大于、小于、范围）',
  html_type         varchar(200)                               comment '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  dict_type         varchar(200)    default ''                 comment '字典类型',
  sort              int                                        comment '排序',
  create_by         varchar(64)     default ''                 comment '创建者',
  create_time 	    datetime                                   comment '创建时间',
  update_by         varchar(64)     default ''                 comment '更新者',
  update_time       datetime                                   comment '更新时间',
  primary key (column_id)
) engine=innodb auto_increment=1 comment = '代码生成业务表字段';

DROP TABLE IF EXISTS st01_crm_customer;
CREATE TABLE st01_crm_customer(
   `customer_id` VARCHAR(128) NOT NULL   COMMENT '客户号' ,
   `customer_name` VARCHAR(256)    COMMENT '客户名称' ,
   `nature` VARCHAR(10)    COMMENT '客户性质;1：企业客户2：个人客户3：政府单位4：事业单位' ,
   `source` VARCHAR(10)    COMMENT '客户来源;1：电话咨询2：自主开拓3：公司资源4：网络推广5：渠道代理6：其他方式' ,
   `industry` VARCHAR(10)    COMMENT '客户行业;1：金融2：电信3：教育4：高科技5：政府6：制造业7：服务业8：能源9：零售10：媒体11：娱乐12：咨询13：非盈利事业14：公用事业15：其他' ,
   `grade` VARCHAR(10)    COMMENT '客户分级;1：大型2：中型3：小型4：微型' ,
   `stage` VARCHAR(10)    COMMENT '客户阶段;1：初步接洽2：确认需求3：解决方案4：商务谈判5：成交6：流失' ,
   `website` VARCHAR(256)    COMMENT '客户官网' ,
   `type` VARCHAR(10)    COMMENT '客户类型;1：最终客户2：渠道客户3：竞争对手' ,
   `introduction` VARCHAR(512)    COMMENT '客户简介' ,
   `importance` VARCHAR(10)    COMMENT '重要程度' ,
   `lock_status` VARCHAR(10)    COMMENT '锁定状态;0：解锁1：锁定' ,
   `address` VARCHAR(512)    COMMENT '详细地址' ,
   `province_id` VARCHAR(128)    COMMENT '省' ,
   `city_id` VARCHAR(128)    COMMENT '市' ,
   `area_id` VARCHAR(128)    COMMENT '区' ,
   `owner_user_id` VARCHAR(128)    COMMENT '销售负责人' ,
   `sa_user_id` VARCHAR(128)    COMMENT '售后负责人' ,
   `deltag` VARCHAR(10)    COMMENT '删除标记;0：未删除1：已删除' ,
   `next_contact_time` VARCHAR(19)    COMMENT '下次联系时间' ,
   `remark` VARCHAR(512)    COMMENT '备注' ,
   `created_by` VARCHAR(32)    COMMENT '创建人' ,
   `created_time` VARCHAR(19)    COMMENT '创建时间' ,
   `updated_by` VARCHAR(32)    COMMENT '更新人' ,
   `updated_time` VARCHAR(19)    COMMENT '更新时间' ,
   PRIMARY KEY (customer_id)
) engine=innodb COMMENT = '客户表';

DROP TABLE IF EXISTS st01_crm_contacts;
CREATE TABLE st01_crm_contacts(
    `contacts_id` VARCHAR(128) NOT NULL   COMMENT '联系人编号' ,
    `contacts_name` VARCHAR(256)    COMMENT '姓名' ,
    `mobile` VARCHAR(128)    COMMENT '手机' ,
    `telephone` VARCHAR(128)    COMMENT '电话' ,
    `department` VARCHAR(128)    COMMENT '部门' ,
    `duties` VARCHAR(128)    COMMENT '职务' ,
    `sex` VARCHAR(10)    COMMENT '性别;0 未知 1 男 2 女' ,
    `birthday` VARCHAR(128)    COMMENT '生日' ,
    `mailbox` VARCHAR(128)    COMMENT '邮箱' ,
    `weChat` VARCHAR(56)    COMMENT '微信号' ,
    `qq` VARCHAR(56)    COMMENT 'QQ号' ,
    `address` VARCHAR(256)    COMMENT '详细地址' ,
    `intimacy` VARCHAR(10)    COMMENT '亲密度' ,
    `important` VARCHAR(10)    COMMENT '重要程度' ,
    `next_contact_time` VARCHAR(19)    COMMENT '下次联系时间' ,
    `dc` VARCHAR(10)    COMMENT '是否为默认联系人;0是1否' ,
    `dm` VARCHAR(10)    COMMENT '是否为关键决策人;0是1否' ,
    `customer_id` VARCHAR(128)    COMMENT '客户ID' ,
    `deltag` VARCHAR(10)    COMMENT '删除标记;0未删除1已删除' ,
    `remark` VARCHAR(256)    COMMENT '备注' ,
    `owner_user_id` VARCHAR(128)    COMMENT '负责人' ,
    `created_by` VARCHAR(32)    COMMENT '创建人' ,
    `created_time` VARCHAR(19)    COMMENT '创建时间' ,
    `updated_by` VARCHAR(32)    COMMENT '更新人' ,
    `updated_time` VARCHAR(19)    COMMENT '更新时间' ,
    PRIMARY KEY (contacts_id)
) engine=innodb COMMENT = '联系人';

DROP TABLE IF EXISTS st01_crm_business;
CREATE TABLE st01_crm_business(
    `business_id` VARCHAR(128) NOT NULL   COMMENT '商机编号' ,
    `business_name` VARCHAR(50)    COMMENT '商机名称' ,
    `money` DECIMAL(18,2)    COMMENT '商机金额' ,
    `status_id` VARCHAR(10)    COMMENT '商机阶段;1：初步沟通2：需求确定3：方案报价4：谈判协商5：' ,
    `next_time` VARCHAR(19)    COMMENT '下次联系时间' ,
    `customer_id` VARCHAR(128)    COMMENT '客户ID' ,
    `deal_date` VARCHAR(19)    COMMENT '预计成交日期' ,
    `discount_rate` DECIMAL(10,2)    COMMENT '整单折扣' ,
    `total_price` DECIMAL(10,2)    COMMENT '产品总金额' ,
    `source` VARCHAR(10)    COMMENT '商机来源;1：独立开发2：来电咨询3：广告宣传4：搜索引擎5：客户介绍6：展会7：招标公告8：其他' ,
    `remark` VARCHAR(500)    COMMENT '备注' ,
    `ro_user_id` VARCHAR(10)    COMMENT '只读权限' ,
    `rw_user_id` VARCHAR(10)    COMMENT '读写权限' ,
    `is_end` VARCHAR(10)   DEFAULT 0 COMMENT '1赢单2输单3无效' ,
    `created_by` VARCHAR(128)    COMMENT '创建人' ,
    `owner_user_id` VARCHAR(128)    COMMENT '负责人ID' ,
    `created_time` VARCHAR(19)    COMMENT '创建时间' ,
    `updated_by` VARCHAR(128)    COMMENT '更新人' ,
    `updated_time` VARCHAR(19)    COMMENT '更新时间' ,
    PRIMARY KEY (business_id)
) engine=innodb COMMENT = '商机表';

DROP TABLE IF EXISTS st01_crm_business_change;
CREATE TABLE st01_crm_business_change(
    `change_id` VARCHAR(128) NOT NULL   COMMENT '变化ID' ,
    `business_id` VARCHAR(128)    COMMENT '商机ID' ,
    `status_id` VARCHAR(10)    COMMENT '阶段ID' ,
    `created_time` VARCHAR(19)    COMMENT '创建时间' ,
    `created_by` VARCHAR(128)    COMMENT '创建人' ,
    PRIMARY KEY (change_id)
) engine=innodb COMMENT = '商机阶段变化表';

DROP TABLE IF EXISTS st01_crm_business_product;
CREATE TABLE st01_crm_business_product(
    `r_id` VARCHAR(128) NOT NULL   COMMENT 'ID' ,
    `business_id` VARCHAR(128)    COMMENT '商机ID' ,
    `product_id` VARCHAR(128)    COMMENT '产品ID' ,
    `price` DECIMAL(18,2)    COMMENT '产品单价' ,
    `sales_price` DECIMAL(18,2)    COMMENT '销售价格' ,
    `num` VARCHAR(10)    COMMENT '数量' ,
    `discount` DECIMAL(10,2)    COMMENT '折扣' ,
    `subtotal` DECIMAL(18,2)    COMMENT '小计（折扣后价格）' ,
    `unit` VARCHAR(10)    COMMENT '单位' ,
    PRIMARY KEY (r_id)
) engine=innodb COMMENT = '商机产品关系表';

DROP TABLE IF EXISTS st01_crm_contacts_business;
CREATE TABLE st01_crm_contacts_business(
    `id` VARCHAR(128) NOT NULL   COMMENT 'ID' ,
    `business_id` VARCHAR(128)    COMMENT '商机ID' ,
    `contacts_id` VARCHAR(128)    COMMENT '联系人ID' ,
    PRIMARY KEY (id)
) engine=innodb COMMENT = '商机联系人关联表';

DROP TABLE IF EXISTS st01_crm_contract;
CREATE TABLE st01_crm_contract(
    `contract_id` VARCHAR(128) NOT NULL   COMMENT '合同编号' ,
    `contract_name` VARCHAR(256)    COMMENT '合同名称' ,
    `money` DECIMAL(18,2)    COMMENT '合同金额' ,
    `customer_id` VARCHAR(128)    COMMENT '客户ID' ,
    `contacts_id` VARCHAR(128)    COMMENT '客户签约人;联系人id' ,
    `business_id` VARCHAR(128)    COMMENT '商机ID' ,
    `types` VARCHAR(10)    COMMENT '合同类型;1：新合同2：续约合同' ,
    `order_date` VARCHAR(19)    COMMENT '下单日期' ,
    `start_time` VARCHAR(19)    COMMENT '开始时间' ,
    `end_time` VARCHAR(19)    COMMENT '结束时间' ,
    `check_status` VARCHAR(10)   DEFAULT 0 COMMENT '状态;0 未审核 1 审核通过 2 审核拒绝 3 审核中 4 已撤回 5草稿 6 作废' ,
    `examine_record_id` VARCHAR(128)    COMMENT '审核记录ID' ,
    `discount_rate` DECIMAL(10,2)    COMMENT '整单折扣' ,
    `total_price` DECIMAL(10,2)    COMMENT '产品总金额' ,
    `payment_type` VARCHAR(10)    COMMENT '付款方式' ,
    `ro_user_id` VARCHAR(10)    COMMENT '只读权限' ,
    `rw_user_id` VARCHAR(10)    COMMENT '读写权限' ,
    `remark` VARCHAR(512)    COMMENT '备注' ,
    `updated_by` VARCHAR(128)    COMMENT '更新人' ,
    `created_by` VARCHAR(128)    COMMENT '创建人' ,
    `owner_user_id` VARCHAR(19)    COMMENT '负责人' ,
    `created_time` VARCHAR(19)    COMMENT '创建时间' ,
    `updated_time` VARCHAR(128)    COMMENT '更新时间' ,
    PRIMARY KEY (contract_id)
) engine=innodb COMMENT = '合同表';

DROP TABLE IF EXISTS st01_crm_contract_product;
CREATE TABLE st01_crm_contract_product(
    `r_id` VARCHAR(128) NOT NULL   COMMENT 'ID' ,
    `contract_id` VARCHAR(128)    COMMENT '合同ID' ,
    `product_id` VARCHAR(128)    COMMENT '产品ID' ,
    `price` DECIMAL(18,2)    COMMENT '产品单价' ,
    `sales_price` DECIMAL(18,2)    COMMENT '销售价格' ,
    `num` VARCHAR(10)    COMMENT '数量' ,
    `discount` DECIMAL(10,2)    COMMENT '折扣' ,
    `subtotal` DECIMAL(18,2)    COMMENT '小计（折扣后价格）' ,
    `unit` VARCHAR(50)    COMMENT '单位' ,
    PRIMARY KEY (r_id)
) engine=innodb COMMENT = '合同产品关系表';

DROP TABLE IF EXISTS st01_crm_leads;
CREATE TABLE st01_crm_leads(
    `leads_id` VARCHAR(128) NOT NULL   COMMENT '线索ID' ,
    `customer` VARCHAR(256)    COMMENT '客户名称' ,
    `nature` VARCHAR(10)    COMMENT '客户性质;1：企业客户2：个人客户3：政府单位4：事业单位' ,
    `source` VARCHAR(10)    COMMENT '客户来源;1：电话咨询2：自主开拓3：公司资源4：网络推广5：渠道代理6：其他方式' ,
    `industry` VARCHAR(10)    COMMENT '客户行业;1：金融2：电信3：教育4：高科技5：政府6：制造业7：服务业8：能源9：零售10：媒体11：娱乐12：咨询13：非盈利事业14：公用事业15：其他' ,
    `grade` VARCHAR(10)    COMMENT '客户分级;1：大型2：中型3：小型4：微型' ,
    `website` VARCHAR(256)    COMMENT '客户官网' ,
    `introduction` VARCHAR(512)    COMMENT '客户简介' ,
    `province_id` VARCHAR(128)    COMMENT '省' ,
    `city_id` VARCHAR(128)    COMMENT '市' ,
    `area_id` VARCHAR(128)    COMMENT '区' ,
    `contacts` VARCHAR(256)    COMMENT '联系人' ,
    `sex` VARCHAR(10)    COMMENT '性别;0 未知 1 男 2 女' ,
    `mailbox` VARCHAR(128)    COMMENT '邮箱' ,
    `is_transform` VARCHAR(10)   DEFAULT 0 COMMENT '是否转为客户;1已转化 0 未转化' ,
    `followup` VARCHAR(10)    COMMENT '跟进状态;0未跟进1已跟进' ,
    `next_time` VARCHAR(19)    COMMENT '下次联系时间' ,
    `telephone` VARCHAR(20)    COMMENT '电话' ,
    `address` VARCHAR(500)    COMMENT '地址' ,
    `remark` VARCHAR(50)    COMMENT '备注' ,
    `created_by` VARCHAR(128)    COMMENT '创建人' ,
    `owner_user_id` VARCHAR(128)    COMMENT '负责人' ,
    `created_time` VARCHAR(19)    COMMENT '创建时间' ,
    `updated_by` VARCHAR(128)    COMMENT '更新人' ,
    `updated_time` VARCHAR(19)    COMMENT '更新时间' ,
    PRIMARY KEY (leads_id)
) engine=innodb COMMENT = '线索表';

DROP TABLE IF EXISTS st01_crm_owner_record;
CREATE TABLE st01_crm_owner_record(
    `record_id` VARCHAR(128) NOT NULL   COMMENT 'ID' ,
    `type_id` VARCHAR(128)    COMMENT '对象id' ,
    `type` VARCHAR(10)    COMMENT '对象类型' ,
    `pre_owner_user_id` VARCHAR(128)    COMMENT '上一负责人' ,
    `post_owner_user_id` VARCHAR(128)    COMMENT '接手负责人' ,
    `created_time` VARCHAR(19)    COMMENT '创建时间' ,
    PRIMARY KEY (record_id)
) engine=innodb COMMENT = '负责人变更记录表';

DROP TABLE IF EXISTS st01_crm_product;
CREATE TABLE st01_crm_product(
    `product_id` VARCHAR(128) NOT NULL   COMMENT '产品ID',
   `product_name` VARCHAR(256)    COMMENT '产品名称' ,
   `num` VARCHAR(128)    COMMENT '产品编码' ,
   `unit` VARCHAR(10)    COMMENT '单位' ,
   `price` DECIMAL(18,2)    COMMENT '价格' ,
   `status` VARCHAR(10)   DEFAULT 0 COMMENT '状态;1 上架 0 下架 3 删除' ,
   `category_id` VARCHAR(128)    COMMENT '产品分类ID' ,
   `description` VARCHAR(512)    COMMENT '产品描述' ,
   `created_by` VARCHAR(128)    COMMENT '创建人' ,
   `updated_by` VARCHAR(128)    COMMENT '更新人' ,
   `create_time` VARCHAR(19)    COMMENT '创建时间' ,
   `update_time` VARCHAR(19)    COMMENT '更新时间' ,
   PRIMARY KEY (product_id)
) engine=innodb COMMENT = '产品表';

DROP TABLE IF EXISTS st01_crm_product_category;
CREATE TABLE st01_crm_product_category(
    `category_id` VARCHAR(128) NOT NULL   COMMENT '产品ID' ,
    `category_name` VARCHAR(200)    COMMENT '' ,
    `sort` VARCHAR(10)   DEFAULT 0 COMMENT '' ,
    `created_by` VARCHAR(32)    COMMENT '创建人' ,
    `created_time` VARCHAR(19)    COMMENT '创建时间' ,
    `updated_by` VARCHAR(32)    COMMENT '更新人' ,
    `updated_time` VARCHAR(19)    COMMENT '更新时间' ,
    PRIMARY KEY (category_id)
) engine=innodb COMMENT = '产品分类表';

DROP TABLE IF EXISTS st01_crm_receivables;
CREATE TABLE st01_crm_receivables(
    `receivables_id` VARCHAR(128) NOT NULL   COMMENT '回款ID' ,
    `number` VARCHAR(128)    COMMENT '回款编号' ,
    `plan_id` VARCHAR(128)    COMMENT '回款计划ID' ,
    `customer_id` VARCHAR(128)    COMMENT '客户ID' ,
    `contract_id` VARCHAR(128)    COMMENT '合同ID' ,
    `return_time` VARCHAR(19)    COMMENT '回款日期' ,
    `return_type` VARCHAR(10)    COMMENT '回款方式' ,
    `money` DECIMAL(18,2)    COMMENT '回款金额' ,
    `remark` VARCHAR(500)    COMMENT '备注' ,
    `check_status` VARCHAR(10)    COMMENT '状态;0 未审核 1 审核通过 2 审核拒绝 3 审核中 4 已撤回' ,
    `examine_record_id` VARCHAR(128)    COMMENT '审核记录ID' ,
    `created_by` VARCHAR(128)    COMMENT '创建人' ,
    `owner_user_id` VARCHAR(128)    COMMENT '负责人' ,
    `created_time` VARCHAR(19)    COMMENT '创建时间' ,
    `updated_by` VARCHAR(128)    COMMENT '更新人' ,
    `updated_time` VARCHAR(19)    COMMENT '更新时间' ,
    PRIMARY KEY (receivables_id)
) engine=innodb COMMENT = '回款表';

DROP TABLE IF EXISTS st01_crm_receivables_plan;
CREATE TABLE st01_crm_receivables_plan(
    `plan_id` VARCHAR(128) NOT NULL   COMMENT '回款计划ID' ,
    `num` VARCHAR(10)    COMMENT '期数' ,
    `receivables_id` VARCHAR(128)    COMMENT '回款ID' ,
    `contract_id` VARCHAR(128)    COMMENT '合同ID' ,
    `customer_id` VARCHAR(128)    COMMENT '客户ID' ,
    `status` VARCHAR(10)    COMMENT '状态;1完成 0 未完成' ,
    `money` DECIMAL(18,2)    COMMENT '计划回款金额' ,
    `return_date` VARCHAR(10)    COMMENT '计划回款日期' ,
    `return_type` VARCHAR(256)    COMMENT '计划回款方式;1：支付宝2：微信3：银行卡转账4：对公转账5：支票6：现金7：其他' ,
    `remind` VARCHAR(10)    COMMENT '提前几天提醒;1：提前一天2：提前两天3：提前五天4：提前七天' ,
    `remind_date` VARCHAR(19)    COMMENT '提醒日期' ,
    `remark` VARCHAR(500)    COMMENT '备注' ,
    `created_by` VARCHAR(128)    COMMENT '创建人' ,
    `owner_user_id` VARCHAR(128)    COMMENT '负责人' ,
    `created_time` VARCHAR(19)    COMMENT '创建时间' ,
    `updated_by` VARCHAR(128)    COMMENT '更新人' ,
    `updated_time` VARCHAR(19)    COMMENT '更新时间' ,
    PRIMARY KEY (plan_id)
) engine=innodb COMMENT = '回款计划表';

DROP TABLE IF EXISTS st01_crm_record;
CREATE TABLE st01_crm_record(
  `record_id` VARCHAR(128) NOT NULL   COMMENT '跟进记录ID' ,
  `types` VARCHAR(10)    COMMENT '关联类型;1:商机2：合同3：回款计划4：汇款单' ,
  `types_id` VARCHAR(10)    COMMENT '类型ID' ,
  `content` VARCHAR(1000)    COMMENT '跟进内容' ,
  `category` VARCHAR(10)    COMMENT '跟进类型;1：外出拜访2：客户面谈3：客户来电4：客户来访5：在线交流6：发送资料7：售后回访8：方案整理9：其他方面' ,
  `business_ids` VARCHAR(128)    COMMENT '商机ID' ,
  `contacts_ids` VARCHAR(128)    COMMENT '联系人ID' ,
  `customer_id` VARCHAR(255)    COMMENT '客户ID' ,
  `next_time` VARCHAR(19)    COMMENT '下次联系时间' ,
  `record_time` VARCHAR(19)    COMMENT '本次跟进时间' ,
  `created_time` VARCHAR(19)    COMMENT '创建时间' ,
  `updated_time` VARCHAR(19)    COMMENT '更新时间' ,
  `updated_by` VARCHAR(128)    COMMENT '更新人' ,
  `created_by` VARCHAR(128)    COMMENT '创建人' ,
  PRIMARY KEY (record_id)
) engine=innodb COMMENT = '跟进记录';